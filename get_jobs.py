import sqlite3
import datetime
import sys
import getpass

from jenkinsapi.jenkins import Jenkins


def get_jobs(jenkins_url, jenkins_username, jenkins_password):
    server = Jenkins(jenkins_url, username=jenkins_username, password=jenkins_password)
    return server.get_jobs()

def insert_jobs_in_database(jenkins_url, jenkins_username, jenkins_password, database):
    jobs = get_jobs(jenkins_url, jenkins_username, jenkins_password)

    conn = sqlite3.connect(database)
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS jobs (name TEXT, is_running INTEGER, 
        is_enabled INTEGER, is_queued INTEGER, check_date TEXT)''')

    now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    for job_name, job_instance in jobs:
        is_running = 1 if job_instance.is_running == True else 0
        is_enabled = 1 if job_instance.is_enabled == True else 0
        is_queued = 1 if job_instance.is_queued == True else 0

        # check if job name exists in the database
        c.execute('SELECT COUNT(*) FROM jobs WHERE name = ?', (job_instance.name,))

        # if job name exists, update
        if c.fetchone()[0] > 0:
            c.execute('''UPDATE jobs SET is_running = ?, is_enabled = ?, is_queued = ?, 
                check_date = ? WHERE name = ?''', 
                (is_running, is_enabled, is_queued, now, job_instance.name))
        # if job name not exists, insert
        else:
            c.execute('INSERT INTO jobs VALUES (?, ?, ?, ?, ?)', 
                (job_instance.name, is_running, is_enabled, is_queued, now))
    
    conn.commit()
    conn.close()
    print 'Jobs inserted'

def main():
    args = sys.argv[1:]
    if len(args) < 1:
        print 'usage: jenkins_url';
        sys.exit(1)

    jenkins_url = args[0]

    username = raw_input('Your Jenkins Username: ')
    password = getpass.getpass('Your Jenskins Password: ')
    database = raw_input('SQLite Database (where to store the Jobs details): ')

    insert_jobs_in_database(jenkins_url, username, password, database)

if __name__ == '__main__':
    main()